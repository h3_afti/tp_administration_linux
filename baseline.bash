#!/bin/bash

_red=$(tput setaf 1)
_green=$(tput setaf 76)
_blue=$(tput setaf 38)
_reset=$(tput setaf 7)

function _checkRootUser()
{
    if (( $EUID != 0 )); then
        echo "Please run as root or use sudo"
        exit 1
    fi
}

function _debug()
{
    if [[ $DEBUG == 1 ]]; then
        # you can choose between 
        set -e
        set -x
        #trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
        #trap 'echo "\"${last_command}\" command ended with exit code $?."' EXIT
    fi
}

function _interact()
{
    printf "${_blue} %s${_reset}\n" "$@"
}

function _printSuccessMessage()
{
    _success "System configured successfully!"

    echo "################################################################"
    echo ""
    echo "     You can now add a new website by using addsite script"
    echo "    You can also remove a website by using removesite script"
    echo ""
    echo "################################################################"
}

function _printUsage()
{
    echo -n "$(basename $0) [OPTION]...
Run this script to prepare a system providing ondemand web services: web server with ftp access.

Version $VERSION

Options:
optional:
-H, --dbhost      MySQL Host
-h, --help        Display this help and exit
--debug           Activate debugging

Examples:
$(basename $0) --help
"
    exit 1
}

function _success()
{
    printf "${_green} %s${_reset}\n" "$@"
}

function ConfigureFtpService()
{
    # vsftpd configuration
    # backup vsftpd configuration
    [[ ! -f /etc/vsftpd.conf.bkp ]] && cp /etc/vsftpd.conf /etc/vsftpd.conf.bkp

    # create ftpsecure user to run the vsftpd service if doesn't exists
    [[ ! `id -u ftpsecure 2>/dev/null` ]] && useradd -r -s /bin/false ftpsecure

    # configuring vsftpd
    sed -i -e 's/#write_enable=YES/write_enable=YES/g' \
    -e 's/#local_umask=022/local_umask=027/g' \
    -e 's/#nopriv_user=ftpsecure/nopriv_user=ftpsecure/g' \
    -e 's/#ftpd_banner=Welcome to blah FTP service./ftpd_banner=Welcome to FTP service./g' \
    -e 's/#chroot_local_user=YES/chroot_local_user=YES/g' \
    -e 's/ssl_enable=NO/ssl_enable=YES/g' /etc/vsftpd.conf

    cat >> /etc/vsftpd.conf <<\EOF
userlist_enable=YES
userlist_file=/etc/vsftpd.userlist
userlist_deny=NO
allow_writeable_chroot=YES
EOF

    touch /etc/vsftpd.userlist

    systemctl restart vsftpd
}

function ConfigureSQLService()
{
    rootPassword=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1`

    # initial mariadb configuration
    # Make sure that nobody can access the server without a password
    SQL1="UPDATE mysql.user SET Password=PASSWORD('${rootPassword}') WHERE User='root';"
    # Remove the anonymous users
    SQL2="DELETE FROM mysql.user WHERE User='';"
    # disallow remote login for root
    SQL3="DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
    # Remove the demo database
    SQL4="DROP DATABASE IF EXISTS test;"
    SQL5="DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';"
    # Apply changes
    SQL6="FLUSH PRIVILEGES;"

    mysql -h $DB_HOST -uroot -f -e "${SQL1}${SQL2}${SQL3}${SQL4}${SQL5}${SQL6}"
	_interact "Mysql root password is: ${rootPassword}"
}

function ConfigureWebServices()
{
    # nginx configuration
    sed -i 's/# server_tokens off;/server_tokens off;/' /etc/nginx/nginx.conf
    sed -i 's/ssl_protocols TLSv1 TLSv1.1/ssl_protocols/' /etc/nginx/nginx.conf
	
	# copy our ssl conf to nginx dir
	[[ ! -f /etc/nginx/conf.d/ssl.conf ]] && mv etc/nginx/conf.d/ssl.conf /etc/nginx/conf.d/ssl.conf

	# Create nginx dir that will host our ssl certs and one that will archive our logs
	[[ ! -d /etc/nginx/certs ]] && mkdir /etc/nginx/certs
	[[ ! -d /var/log/nginx/archives ]] && mkdir /var/log/nginx/archives

	# stronger ssl configuration
	[[ ! -f /etc/nginx/certs/dhparam.pem ]] && _interact "Generating Diffie Hellman file, this may take a while. Please wait" && openssl dhparam -out /etc/nginx/certs/dhparam.pem 4096 2>/dev/null

    # create default website as showcase
    [[ -f /etc/nginx/sites-enabled/default ]] && rm /etc/nginx/sites-enabled/default
    [[ -f /etc/nginx/sites-available/default ]] && mv /etc/nginx/sites-available/default /etc/nginx/sites-available/defaultwebsite.conf
    [[ ! -f /etc/nginx/sites-enabled/defaultwebsite.conf ]] && ln -s /etc/nginx/sites-available/defaultwebsite.conf /etc/nginx/sites-enabled/defaultwebsite.conf

    # baseline websites configuration; don't test if exists because of future updates
    cp etc/nginx/sites-available/wordpress.conf /etc/nginx/sites-available/wordpress.conf
    cp etc/nginx/sites-available/joomla.conf /etc/nginx/sites-available/joomla.conf
    cp etc/nginx/sites-available/noCMS.conf /etc/nginx/sites-available/noCMS.conf

    # php configuration
    sed -i "s/memory_limit = .*/memory_limit = 512M/" /etc/php/7.3/fpm/php.ini
    sed -i "s/upload_max_filesize = .*/upload_max_filesize = 128M/" /etc/php/7.3/fpm/php.ini
    sed -i "s/post_max_size = .*/post_max_size = 128M/" /etc/php/7.3/fpm/php.ini
    sed -i "s/max_execution_time = .*/max_execution_time = 3000/" /etc/php/7.3/fpm/php.ini

	# Create nginx dir that will host our ssl certs and one that will archive our logs
	[[ ! -d /var/log/php/archives ]] && mkdir -p /var/log/php/archives

    # baseline pool creation
    cp etc/php/fpm/defaultpool.conf /etc/php/7.3/fpm/defaultpool.conf

    # Restart service to apply changes
    systemctl restart php7.3-fpm
    systemctl restart nginx
}

function ProcessArgs()
{
	while getopts ":H-:" optchar; do
		case "${optchar}" in
			-)
				case "${OPTARG}" in
					dbhost)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						[[ -z $DB_HOST ]] && DB_HOST="${val}" || DB_HOST=$DB_HOST,"${val}"
						;;
					dbhost=*)
						val=${OPTARG#*=}
						[[ -z $DB_HOST ]] && DB_HOST="${val}" || DB_HOST=$DB_HOST,"${val}"
						;;
					debug)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						DEBUG=1
						;;
					debug=*)
						val=${OPTARG#*=}
						DEBUG=1
						;;
					*)
						_printUsage
				esac;;
			H)
				val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
				[[ -z $DB_HOST ]] && DB_HOST="${val}" || DB_HOST=$DB_HOST,"${val}"
				;;
			*)
				_printUsage
				;;
		esac
	done
    [[ -z $DB_HOST ]] && DB_HOST="localhost"
}

function ServicesInstall()
{
    # System update
    apt -qq update
    # Services installation. haveged is use for entropy generation.
    apt-get install -y openssh-server dnsutils curl nginx php7.3-fpm php7.3-mysql php7.3-xml php7.3-curl php7.3-cli php7.3-gd php7.3-opcache php7.3-json mariadb-server vsftpd haveged  > /dev/null
}

function SystemConfiguration()
{
    # System configuration
    # files/directories should only be modified by user, read by group but that's all!
    sed -i "/^UMASK/c\UMASK 027" /etc/login.defs
    grep -q '^umask 027' /etc/profile || echo 'umask 027' >> /etc/profile
	echo "umask = 027" >> /etc/skel/.bashrc

    # Skel configuration depending of CMS used
    # joomla skel
    [[ ! -d /etc/skel.joomla/ ]] && cp -r /etc/skel /etc/skel.joomla
    [[ ! -f /etc/skel.joomla/index.php ]] && wget -qO- https://downloads.joomla.org/fr/cms/joomla3/3-9-16/Joomla_3-9-16-Stable-Full_Package.tar.gz?format=gz | tar xz -C /etc/skel.joomla/

    # wordpress skel
    [[ ! -d /etc/skel.wordpress/ ]] && cp -r /etc/skel /etc/skel.wordpress
    [[ ! -f /etc/skel.wordpress/index.php ]] && wget -qO- https://fr.wordpress.org/latest-fr_FR.tar.gz | tar xz -C /tmp/ && mv /tmp/wordpress/* /etc/skel.wordpress/

    # index.html created in default skel
    cat > /etc/skel/index.html <<\EOF
<!doctype html>
<html>
<head>
<title>Documentation is like sex. When it's good, it's very good. When it's bad, it's better than nothing</title>
</head>
<body>
<p><a href="https://miro.medium.com/proxy/1*MQ4qfrgjdpvLjjiu-HUH7A.jpeg"><img src="https://miro.medium.com/proxy/1*MQ4qfrgjdpvLjjiu-HUH7A.jpeg" alt="" width="650" height="434" /></a></p>
</body>
</html>
EOF

    # copy addsite and removesite scripts so that we can call them as command
    cp usr/local/bin/addsite.bash /usr/local/bin/addsite.bash
    cp usr/local/bin/removesite.bash /usr/local/bin/removesite.bash
    chmod +x /usr/local/bin/addsite.bash
    chmod +x /usr/local/bin/removesite.bash

    # Create file to list users and website so that we can easily know what is hosted here ;)
    touch /etc/Users_Websites.txt
}

#### MAIN ####
function main()
{
    _success "Configuring system"
    SystemConfiguration
    _success "Done!"

    _success "Installing nginx, php7.3, mariadb-server and vsftpd services"
    ServicesInstall
    _success "Done!"

    _success "Configuring ftp service..."
    ConfigureFtpService
    _success "Done!"

    _success "Configuring SQL service..."
    ConfigureSQLService
    _success "Done!"

    _success "Configuring web services..."
    ConfigureWebServices
    _success "Done!"

    _printSuccessMessage

    exit 0
}

VERSION="1.0"
PATH="$PATH:/usr/sbin"

_checkRootUser

ProcessArgs "$@"

_debug

main "$@"
