upstream php$FILENAME {
server unix:/run/php/$FILENAME.sock;
}

server {
	listen 80;
	server_name $VHOST;
	return 301 https://$server_name$request_uri;
}

server {
	listen 443 ssl http2;
	server_name $VHOST;

	root $ROOTDIR;

	index index.php index.html index.htm;

	access_log /var/log/nginx/$FILENAME.access.log;
	error_log /var/log/nginx/$FILENAME.error.log info;

	ssl_certificate /etc/nginx/certs/$FILENAME.crt;
	ssl_certificate_key /etc/nginx/certs/$FILENAME.key;

	location / {
		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		try_files $uri $uri/ =404;
	}

	# pass PHP scripts to FastCGI server
	location ~ \.php$ {
		fastcgi_pass php$FILENAME;
		fastcgi_index index.php;
		fastcgi_buffering off;
		include fastcgi_params;
		fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
		include /etc/nginx/fastcgi.conf;
	}

	location = /favicon.ico {
		log_not_found off;
		access_log off;
	}

	location = /robots.txt {
		allow all;
		log_not_found off;
		access_log off;
	}
}