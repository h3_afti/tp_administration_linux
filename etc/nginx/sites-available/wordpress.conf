upstream php$FILENAME {
	server unix:/run/php/$FILENAME.sock;
}

server {
	listen 80;
	server_name $VHOST;
	return 301 https://$server_name$request_uri;
}

server {
	listen 443 ssl http2;
	server_name $VHOST;

	root $ROOTDIR;

	index index.php;

	access_log /var/log/nginx/$FILENAME.access.log;
	error_log /var/log/nginx/$FILENAME.error.log info;

	ssl_certificate /etc/nginx/certs/$FILENAME.crt;
	ssl_certificate_key /etc/nginx/certs/$FILENAME.key;

	# Global restrictions configuration file.
	# Designed to be included in any server {} block.
	location = /favicon.ico {
		log_not_found off;
		access_log off;
	}

	location = /robots.txt {
		allow all;
		log_not_found off;
		access_log off;
	}

	# Deny all attempts to access hidden files such as .htaccess, .htpasswd, .DS_Store (Mac).
	# Keep logging the requests to parse later (or to pass to firewall utilities such as fail2ban)
	location ~ /\. {
		deny all;
	}

	# Deny access to any files with a .php extension in the uploads directory
	# Works in sub-directory installs and also in multisite network
	# Keep logging the requests to parse later (or to pass to firewall utilities such as fail2ban)
	location ~* /(?:uploads|files)/.*\.php$ {
		deny all;
	}

	location / {
		try_files $uri $uri/ /index.php?$args;
	}

	location ~ \.php$ {
		include fastcgi_params;
		fastcgi_intercept_errors on;
		fastcgi_pass php$FILENAME;
		#The following parameter can be also included in fastcgi_params file
		fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
	}

	location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
		expires max;
		log_not_found off;
	}
}
