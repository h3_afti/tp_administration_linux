# TP_admin_Linux

### Features
- LEMP server based Debian 10 build with security in mind
- Create a wordpress or joomla ready website, or an empty folder for you to build the website of your dreams
- Create a FTP access
- Create a database
- Create a user to connect all services above

### Security Features
##### Nginx
- Separated vhosts
- Server token hidden
- SSL strong and modern configuration
##### Php
- dedicated fpm pools
- fpm process created ondemand
- upload and post sizes limited
- memory usage limited per pool 
- max exectution time set 
##### Mysql
- Hardened root password
- No anonymous user
- No remote root login
- Useless databases removed
- Useless users removed
- End users can only access their database
##### Ftp
- dedicated user to run vsftpd service
- user whitelisting and no anonymous users
- process is chrooted: user dan't access anything but his homedir

### Setup
This setup is intended to be run on a minimal Debian 10.3 netinstall.
- Run `sudo bash baseline.bash` to install prerequisites
- Run `sudo addsite.bash` to add a new website. See details below.
- Run `sudo removesite.bash` to remove a new website. See details below.

### Usage
Run initial configuration script with database located on *mysqlserver* host.\
`sudo bash baseline.bash --dbhost mysqlserver`

Create a new website for user *myuser* with two domain names: *www.test.local* and *test.local* and a remote mysql server.\
`sudo addsite.bash -H mysqlserver --user myuser -d website.tld -d www.website.tld -c joomla`

Remove *myuser* user.\
`sudo remove.bash -H localhost --user myuser -d website.tld`

Run any "$script --help" for detailed usage

### Scripts details
General services configurations are explained in [documentation/configuration.md](documentation/configuration.md).\
Scripts functions are detailed in [documentation/functions.md](documentation/functions.md).

### Improvments
Some changes are planned but won't be done before 2020.04.23. They include but are not limited to
- chroot user to his homedir
- chroot php process
- improve/harden php configuration
- improve/harden nginx configuration
- use letsencrypt acme for ssl generation used by nginx or vsftpd. Can't be tested locally for now
- more hardening system
- These scripts aren't POSIX compliant, they should be... 

That's it! Enjoy :)
