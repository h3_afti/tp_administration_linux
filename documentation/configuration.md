# Rapport du TP administration linux : Automatisation de la création d’un site web.
## Cahier des charges
L'objectif de ce TP est de valider les compétences acquises durant la semaine de formation GNU / Linux.\
Durant ces travaux pratiques, vous devrez mettre en place un serveur GNU / Linux permettant d'héberger des sites Web utilisant les programmes Apache2 / PHP / MySQL et proposant un accès FTP à l'utilisateur.\
La création d'un nouvel accès pour un utilisateur devra se faire via un script Bash à exécuter sur le serveur, celui-ci pourra s'exécuter avec des paramètres ou être interractif. Ce script sera chargé de :
- Créer un nouvel utilisateur Unix dédié au site Web
- Créer un hôte virtuel Apache2 avec le ou les noms de domaine spécifié
- Créer un compte FTP pour l'utilisateur
- Créer une base de donnée et un utilisateur dédié ayant tous les droits sur cette base de donnée exclusivement
- Créer un Pool PHP dédié au site web
- Définir un mot de passe complexe aléatoire
- Recharger la configuration des services nécessaires après l'ajout de l'utilisateur
\
\
Exemple de script attendu : \
create_hosting_account --username alex --vhost example.org --vhost www.example.org \
\
Lors de la création de ce script, il vous sera demandé également de fournir une documentation détaillant le fonctionnement de votre script et la configuration des différents services (nom des programmes utilisés, configuration importantes modifiées...). Rédigez une partie sur les politiques de sécurité que vous avez mis en place pour améliorer le cloisement entre chaque utilisateur et chaque site web lui appartenant, ainsi que sur les différents services (Apache2 / MySQL / FTP ...).

## Réponse au CDC : 
Usage d’un stack LEMP et ajout du service vsftpd pour l’accès ftp aux données du site.
L'usage du service web nginx à été validé par le client.\
Plusieurs scripts permettront de répondra à ce besoin :
- Un script de configuration générale du système configurera les différents services et les préparera à l’ajout ou au retrait automatique de site.
- Un script de création de site web créera l’utilisateur, le site web, la base de données et l’accès ftp
- Un script de retrait de site supprimera l’utilisateur, le site, la base de données, l’accès ftp

Afin de simplifier et s'assurer qu'aucun doublon n'est créé, nos fichiers de configurations et noms de base de données aurons le nom du site web. Il est représenté par ${FILENAME} dans le document.

## Mise en garde
Attention, ce document n'a pas vocation à être un guide pas-à-pas pour créer un site web. Le document est présent pour expliquer la configuration des différents scripts d'installation et ne reprend pas toutes les fonctionnalités de ceux-ci. La gestion du nombre d'hôte virtuel à créer pour un utilisateur n'est pas pris en compte par exemple.

## Configuration du système
Le système doit être modifier pour s'adapter à nos besoins.\
Par mesure de sécurité, nous modifions le masque par défaut des fichiers afin qu’ils ne soient modifiables que par le propriétaire, et lisibles uniquement par le groupe : les autres utilisateurs n'ont ainsi pas accès aux fichiers.
```
sed -i "/^UMASK/c\UMASK 027" /etc/login.defs
grep -q '^umask 027' /etc/profile || echo 'umask 027' >> /etc/profile
echo "umask = 027" >> /etc/skel/.bashrc
```
Nous créons des modèles de profils utilisateurs en fonction de leurs besoins : un profil contenant un site web WordPress, un profil contenant un site web Joomla, et un profil contenant seulement un index.html donné en exemple. \
Exemple pour la création d'un skelette contenant la configuration joomla: 
```
[[ ! -d /etc/skel.joomla/ ]] && cp -r /etc/skel /etc/skel.joomla
[[ ! -f /etc/skel.joomla/index.php ]] && wget -qO- https://downloads.joomla.org/fr/cms/joomla3/3-9-16/Joomla_3-9-16-Stable-Full_Package.tar.gz?format=gz | tar xz -C /etc/skel.joomla/
```
Nous copions nos scripts d'ajout et de suppression de site dans `/usr/local/bin` afin qu'ils soient contenus dans le $PATH des administrateurs et puissent ainsi être exécutés en tant que commande.
```
cp usr/local/bin/addsite.bash /usr/local/bin/addsite.bash
cp usr/local/bin/removesite.bash /usr/local/bin/removesite.bash
chmod +x /usr/local/bin/addsite.bash
chmod +x /usr/local/bin/removesite.bash
```
Le fichier `/etc/Users_Websites.txt` est créé afin d'accueillir nos utilisateurs et sites existants. Ce fichier est modifié à chaque ajout ou retrait de site afin de suivre l'état exact des utilisateurs et sites de notre serveur.
```
touch /etc/Users_Websites.txt
```

## Gestion des utilisateurs
#### Ajout d'utilisateurs
L'ajout d'un utilisateur se fait directement sur le système.\
Chaque utilisateur possède son propre répertoire personnel dans `/home/${NEW_USER}`. Nous laissons le homedir par défaut dans /home par simplicité : un modèle de partitionnement par défaut à l'installation sépare les partitions /home de celle du système. Ainsi, si un utilisateur surcharge son répertoire, le système n'est pas impacté. Ce répertoire a un contenu différent en fonction du site web qu'il souhaite déployer.\
L'utilisateur a pour groupe www-data, le groupe du serveur web. Nos droits d'accès sont donc respectés : le service web peut seulement lire les données de l'utilisateur.\
Nous ajoutons notre utilisateur et site web associé à notre fichier de suivi `/etc/Users_Websites.txt` une fois l'utilisateur créé.
```
useradd -m -s /bin/bash -g www-data -k /etc/skel.joomla ${NEW_USER}
echo -e "${USER_PASS}\n${USER_PASS}" | passwd ${NEW_USER}
echo "${NEW_USER}:${VHOSTS}" >> /etc/Users_Websites.txt
```

#### Retrait d'utilisateurs
La suppression d'un utilisateur se fait directement sur le système. Nous devons d'abord nous assurer que l'ensemble des process qui tournent en son noms soient arrêtés.\
Nous retirons l'utilisateur et le site web associé du fichier de suivi `/etc/Users_Websites.txt` une fois que l'utilisateur est supprimé.
```
killall -u ${NEW_USER}
userdel -r -f ${NEW_USER}
sed -i "/^${NEW_USER}/d" /etc/Users_Websites.txt
```

## Configuration du serveur web
### Service nginx
Le service web sera géré par nginx.\
Voir [https://nginx.org/en/docs/](https://nginx.org/en/docs/)\
Nous installons le service :
```
apt install -y nginx
```

#### Configuration
Par défaut, la version de Nginx est affichée dans le navigateur. Nous cachons cette information par mesure de sécurité.
```
sed -i 's/# server_tokens off;/server_tokens off;/' /etc/nginx/nginx.conf
```
Tous nos sites web seront joignables en https uniquement, il est nécessaire de configurer les paramètres ssl au sein de Nginx: une configuration moderne et sécurisée est appliquée.\
Voir [https://ssl-config.mozilla.org/](https://ssl-config.mozilla.org/) \
Nous commençons par supprimer les anciens protocoles TLS résidant dans la configuration par défaut.
```
sed -i 's/ssl_protocols TLSv1 TLSv1.1/ssl_protocols/' /etc/nginx/nginx.conf
```
Nous ajoutons le fichier *ssl.conf* dans `/etc/nginx/conf.d` pour paramétrer le ssl: \
You can find good documentation on ssl configuration at [https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html](https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html).
```
## Diffie-Hellman
ssl_ecdh_curve secp384r1;
ssl_dhparam /etc/nginx/certs/dhparam.pem;

## Ciphers
ssl_ciphers EECDH+CHACHA20:EECDH+AESGCM:EECDH+AES;

# OCSP Stapling
resolver 80.67.169.12 80.67.169.40 valid=300s;
resolver_timeout 5s;
ssl_stapling on;
ssl_stapling_verify on;

## TLS parameters
ssl_session_cache shared:SSL:10m;
ssl_session_timeout 5m;
ssl_session_tickets off;

## HSTS
# add_header Strict-Transport-Security "max-age=15552000; includeSubdomains; preload";
```
Nous créons le dossier `/etc/nginx/certs` qui accueillera les certificats ssl de nos utilisateurs, puis la clé DH utilisée dans les échanges sécurisés:
```
mkdir /etc/nginx/certs
openssl dhparam -out /etc/nginx/certs/dhparam.pem 4096
```
Le dossier `/var/log/nginx/` contiendra les archives des logs des sites supprimés.
```
mkdir /var/log/nginx/archives
```

Nous devons maintenant créer nos modèles d'hôtes virtuels qui seront utilisés et configurés selon les sites des utilisateurs. \
Voir [etc/nginx/sites-available/wordpress.conf](etc/nginx/sites-available/wordpress.conf) comme exemple \
Chacun des fichiers de configuration fournis proviennent de sources officielles:
- [https://wordpress.org/support/article/nginx/](https://wordpress.org/support/article/nginx/)
- [https://docs.joomla.org/Nginx/fr](https://docs.joomla.org/Nginx/fr)
- [https://www.nginx.com/resources/wiki/start/topics/examples/full/](https://www.nginx.com/resources/wiki/start/topics/examples/full/)
```
cp etc/nginx/sites-available/wordpress.conf /etc/nginx/sites-available/wordpress.conf
```

#### Ajout/Retrait de vhost
Afin d'ajouter un hôte virtuel, nous copions notre modèle vers `/etc/nginx/sites-enabled/${FILENAME}.conf` et nous remplaçons les variables grâce à la commande envsubst:
```
envsubst '${VHOST} ${ROOTDIR} ${FILENAME}' < "/etc/nginx/sites-available/wordpress.conf" > "/etc/nginx/sites-enabled/${FILENAME}.conf"
```
Nous devons aussi créer un certificat ssl correspondant au site web. Nous concevons une configuration temporaire qui sera utilisée par openssl pour générer le certificat de manière automatique. \
Vous pouvez modifier manuellement ces valeurs dans [usr/local/bin/addsite.bash](usr/local/bin/addsite.bash). Sachez néanmoins que ces certificats ne seront pas valides car autosignés. Des certificats ssl valides, créés par [LetsEncrypt](https://letsencrypt.org/fr/) par exemple, doivent être générés si votre serveur web est public. Un mise à jour de ce script est à venir pour définir quel type de certificat générer.
```
certconfigfile=`mktemp`
cat > ${certconfigfile} <<\EOF
[req]
default_bits = 4096
prompt = no
default_md = sha256
req_extensions = req_ext
x509_extensions  = req_ext
distinguished_name = dn

[ dn ]
C=FR
ST=France
L=Paris
O=Security
OU=IT Department
emailAddress=admin
CN = My website

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.${VHOST}
EOF
```
Nous générons le certificat.
```
openssl req -config ${certconfigfile} -new -x509 -sha256 -newkey rsa:4096 -nodes -keyout /etc/nginx/certs/${FILENAME}.key -days 365 -out /etc/nginx/certs/${FILENAME}.crt
```
\
Pour retirer un hôte virtuel, il faut supprimer le fichier de configuration correspondant au site situé dans `/etc/nginx/sites-enabled/`. Il faut aussi penser à archiver les logs situés dans `/var/log/nginx/`.
```
rm -f "/etc/nginx/sites-enabled/${FILENAME}.conf"
mv /var/log/nginx/${FILENAME}.access.log /var/log/nginx/archives/${FILENAME}_${NEW_USER}.access.log`date '+%Y-%m-%d_%H:%M:%S'`
mv /var/log/nginx/${FILENAME}.error.log /var/log/nginx/archives/${FILENAME}_${NEW_USER}.error.log`date '+%Y-%m-%d_%H:%M:%S'`
```
Penser à recharger la configuration de Nginx après chaque ajout ou retrait d'hôte virtuel.
```
systemctl reload nginx
```

### Service PHP
PHP-FPM est l'interface basée sur le protocole FastCGI permettant la communication entre notre serveur web et PHP. Nous installons plusieurs modules PHP nécessaires au bon fonctionnement de notre service:
```
apt install -y php7.3-fpm php7.3-mysql php7.3-xml php7.3-curl php7.3-cli php7.3-gd php7.3-opcache php7.3-json
```
#### Configuration
Par sécurité, nous limitons certaines configurations de PHP, notamment la mémoire allouée, la taille maximum des fichiers envoyés sur le serveur ainsi que le temps maximum d'exécution d'un script. Cette modification se fait dans `/etc/php/7.3/fpm/php.ini`.
```
sed -i "s/memory_limit = .*/memory_limit = 512M/" /etc/php/7.3/fpm/php.ini
sed -i "s/upload_max_filesize = .*/upload_max_filesize = 128M/" /etc/php/7.3/fpm/php.ini
sed -i "s/post_max_size = .*/post_max_size = 128M/" /etc/php/7.3/fpm/php.ini
sed -i "s/max_execution_time = .*/max_execution_time = 3000/" /etc/php/7.3/fpm/php.ini
```

Afin de cloisonner chaque site web, chaque hôte virtuel de notre serveur web se verra alloué un pool (un processus). Ce pool sera basé sur le modèle suivant :
```
[$FILENAME]
user = $NEW_USER
group = www-data

listen = /run/php/$FILENAME.sock

listen.owner = $NEW_USER
listen.group = www-data
listen.mode = 0660

pm = ondemand
pm.max_children = 5
pm.process_idle_timeout = 15s;
pm.max_requests = 500

access.log = /var/log/php/$FILENAME.access.log
php_admin_flag[log_errors] = on
php_admin_value[error_log] = /var/log/php/$FILENAME.error.log
```
Chaque socket créé automatiquement dans `/run/php/` aura pour owner notre utilisateur, et pour groupe le groupe web *www-data*. \
Chaque processus fils sera créé à la demande et le nombre maximum de processus fils alloué à ce pool sera de 5 afin de limiter l'usage des ressources. Un processus est signalé comme dormant après 15s d'inactivité. Chaque processus devra renaître après 500 requêtes afin d'éviter les fuites de mémoire. \
Les logs d'accès et d'erreur sont définis afin d'assurer une tracabilité par site web.

#### Ajout/Retrait de pool
Tout comme Nginx, il faut ajouter ou supprimer le fichier de configuration modèle situé dans `/etc/php/7.3/fpm/pool.d/`. \
Ajout :
```
envsubst '${NEW_USER} ${FILENAME}' < "/etc/php/7.3/fpm/defaultpool.conf" > "/etc/php/7.3/fpm/pool.d/${FILENAME}.conf"
```
Suppression :
```
rm -f "/etc/php/7.3/fpm/pool.d/${FILENAME}.conf"
mv /var/log/php/${FILENAME}.access.log /var/log/php/archives/${FILENAME}_${NEW_USER}.access.log`date '+%Y-%m-%d_%H:%M:%S'`
mv /var/log/php/${FILENAME}.error.log /var/log/php/archives/${FILENAME}_${NEW_USER}.error.log`date '+%Y-%m-%d_%H:%M:%S'`
```
Penser à recharger la configuration de php-fpm après chaque ajout ou retrait de pool
```
systemctl reload php7.3-fpm
```

## Configuration de la base de données
La base de données sera gérée par le service mariadb-server.\
Voir [https://mariadb.org/documentation/](https://mariadb.org/documentation/) \
Nous installons mariadb avec la commande
```
apt install -y mariadb-server
```

#### Configuration
L'installation du service mariadb-server ne nécessite pas de configuration particulière pour fonctionner. Pour des raisons de sécurité, nous empêchons les utilisateurs de se connecter sans mot de passe, nous supprimons l'utilisateur anonyme, empêchons l'utilisateur root de se connecter à distance, et supprimons la base de données et l'utilisateur 'test'.
```
    rootPassword=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1`

    SQL1="UPDATE mysql.user SET Password=PASSWORD('${rootPassword}') WHERE User='root';"
    SQL2="DELETE FROM mysql.user WHERE User='';"
    SQL3="DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
    SQL4="DROP DATABASE IF EXISTS test;"
    SQL5="DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';"
    SQL6="FLUSH PRIVILEGES;"

    mysql -h $DB_HOST -uroot -f -e "${SQL1}${SQL2}${SQL3}${SQL4}${SQL5}${SQL6}"
```

#### Création ou retrait d'un site web
Lors de la création d'un site web, une base de données et un utilisateur associé à cette base doivent être créés : \
- nous créons la base de données, \
- nous créons l’utilisateur, \
- nous autorisons l'utilisateur à se connecter à la base de données, \
- nous actualisons la prise en compte des droits.
```
CREATE DATABASE IF NOT EXISTS ${FILENAME};
CREATE USER ${NEW_USER}@'%' IDENTIFIED BY 'mypassword';
GRANT ALL PRIVILEGES ON ${FILENAME}.* TO '${NEW_USER}'@'%';
FLUSH PRIVILEGES;
```
Lors du retrait d'un site web, nous supprimons la base de donnée et l'utilisateur associé :
```
DROP DATABASE IF EXISTS ${FILENAME};
DROP USER IF EXISTS ${NEW_USER}@'%';
FLUSH PRIVILEGES;
```

## Configuration du serveur FTP
L'accès ftp sera géré par le service vsftpd.\
Voir [https://wiki.debian.org/fr/vsftpd](https://wiki.debian.org/fr/vsftpd)
L'installation du service se fait avec la commande `apt install -y vsftpd`

#### Configuration
Par défaut, le service vsftpd tourne en tant que root, pour de raisons de sécurité, nous créons un utilisateur dédié qui lancera le service.
Voici les configurations qui diffèrent du fichier `/etc/vsftpd.conf` par défaut :

```write_enable=YES ``` : Nous autorisons l'ajout de fichier\
```local_umask=027 ``` : Le masque est défini de manière que les fichiers créés ne puissent être modifiés que par l'utilisateur et lus par le groupe.\
```nopriv_user=ftpsecure ``` : Le service est maintenant lancé avec l'utilisateur 'ftpsecure'.\
```ftpd_banner=Welcome to FTP service. ``` : La bannière a été ajoutée.\
```chroot_local_user=YES ``` : Les utilisateurs ne peuvent pas sortir de leur répertoire personnel (chroot). \
```allow_writeable_chroot=YES ``` : Nous empêchons le service vsftpd de vérifier les droits du dossier parent. Par défaut, si le dossier parent est modifiable, vsftpd n'accepte pas de limiter les utilisateurs à leur répertoire personnel. \
```ssl_enable=YES ``` : La paramètre ssl_enable passé à YES impose l'usage du ssl lors des échanges ftp. Nous sécurisons ainsi le trafic et utilisons le protocol ftps au lieu de ftp.\
```userlist_enable=YES ``` et ```userlist_file=/etc/vsftpd.userlist ``` : Créer une liste dans /etc/vsftpd.userlist qui contiendra des utilisateurs\
```userlist_deny=NO ``` : Par défaut, les utilisateurs qui ne sont pas dans la liste ci-dessus ne peuvent s'identifier.

#### Ajout/Retrait d'utilisateurs
L'ajout et le retrait d'utilisateur se fait simplement en ajoutant ou supprimant le nom de l'utilisateur dans le fichier /etc/vsftpd.userlist précisé plus tôt. L'ajout autorisera l'utilisateur à se connecter, et le retrait l'empêchera de se connecter. \
Penser à recharger la configuration de vsftpd après chaque ajout ou retrait d'utilisateur.
```
systemctl reload vsftpd
```