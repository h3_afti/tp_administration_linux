# Functions explanation

We only detail function that aren't part of the [documentation/configuration.md](documentation/configuration.md) explanation.\
We describe simplified used function, their exact content may vary within baseline.bash, addsite.bash or removesite.bash scripts.

## Colorization functions
```
_red=$(tput setaf 1)
_green=$(tput setaf 76)
_blue=$(tput setaf 38)
_reset=$(tput sgr0)
```
`_red` `_green` and `_blue` set font colors. `_reset` reset colorization to white.\
See [https://unix.stackexchange.com/questions/269077/tput-setaf-color-table-how-to-determine-color-codes](https://unix.stackexchange.com/questions/269077/tput-setaf-color-table-how-to-determine-color-codes) for details and examples

## function _checkRootUser()
```
function _checkRootUser()
{
    if (( $EUID != 0 )); then
        echo "Please run as root or use sudo"
        exit 1
    fi
}
```
This function tests if $EUID, the effective user id, differs from 0. If so, it means that we don't have root access or elevated privileged thanks to sudo. This script can't be run: we display a message and exit the script.

## function _debug()
```
function _debug()
{
    if [[ $DEBUG == 1 ]]; then
        set -e
        set -x
        #trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
        #trap 'echo "\"${last_command}\" command ended with exit code $?."' EXIT
    fi
}
```
This function checks if DEBUG variable is set to one. If so, we exit immediately if a command exits with a non-zero status (set -e) and we print commands and their arguments as they are executed (set -x).\
traps aren't used but could replace both `set -e` and `set -x`. DEBUG trap execute custom commands before each line of the script when running and EXIT trap execute custom commands before before the script exits. See [https://linuxconfig.org/how-to-debug-bash-scripts](https://linuxconfig.org/how-to-debug-bash-scripts#h5-how-to-use-trap-to-help-debug) for details about traps.

## function _error()
```
function _error()
{
    printf "${_red} %s${_reset}\n" "$@"
}
```
This function prints message passed in argument ($@ represents all arguments passed to the function) in red then turn color back to white by calling _red and _reset function.

## function _interact()
```
function _interact()
{
    printf "${_blue} %s${_reset}\n" "$@"
}
```
This function prints message passed in argument ($@ represents all arguments passed to the function) in blue then turn color back to white by calling _blue and _reset function.

## function _IsValidIp()
```
function _IsValidIp()
{
    local  ip=$1
    local  stat=1
    # Check if IP looks like an IP: 4 time 3 digits separated by dots
    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        # Internal field separator saved then set as dot so that we can use each ip part as argument
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}
```
This function checks if the IP passed as argument is a valid one.
stat is set to 1 because 1 is an error return code. We then check if ip has the right scheme. We backup $IFS which is an environment variable containing field separator. We set this field separator as '.' so that our ip will have four fields separated by dots with ip=($ip). We then set back $IFS to default and check if each IP field is less than 255. If so, this check returns 0. This return ($?) is registered in stat var. We then return stat var. You can print regexp in [https://regex101.com/](https://regex101.com/) to get a step by step explanation.

## function _printSuccessMessage()
```
function _printSuccessMessage()
{
    _success "Website created successfully!"

    echo "################################################################"
    echo ""
    echo " >> Host      : ${VHOST}"
    echo " >> IP        : ${IP}"
    echo " >> User      : ${NEW_USER}"
    echo " >> Password  : ${USER_PASS}"
    echo " >> Database  : ${FILENAME}"
    echo " >> SQL host  : ${DB_HOST}"
    echo ""
    echo "################################################################"
}
```
This function first calls _success function to display text in green.
We then display some usefull informations about newly created website.

## function _printUsage()
```
function _printUsage()
{
    n "$(basename $0) [OPTION]...

Create new website environment:
create user
create database
create virtualhost
create ftp access
add cms template if specified
Version $VERSION

Options:
mandaroty:
-u, --user        User
-d, --vhost       Domain name
optional:
-H, --host        MySQL Host
-c, --cms         CMS to use (joomla|wordpress)
-i, --ip          Webserver IP address
-h, --help        Display this help and exit
-v, --version     Output version information and exit
--debug           Activate debugging

Examples:
$(basename $0) --help
$(basename $0) -H=localhost -u=myuser -d=website.tld -d=www.website.tld -c joomla
"
    exit 1
}
```
_printUsage function displays the function manual.
unction is called when a misconfiguration is detected so it exits the script with error code 1.

## function _success()
```
function _success()
{
    printf "${_green} %s${_reset}\n" "$@"
}
```
This function prints message passed in argument. ($@ represents all arguments passed to the function) in green then turn color back to white by calling _green and _reset function.
## function ProcessArgs()
```
function ProcessArgs()
{
    [[ $# -lt 2 ]] && _printUsage
	while getopts ":u-:" optchar; do
		case "${optchar}" in
			u)
				val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
				[[ -z $NEW_USER ]] && NEW_USER="${val}" || NEW_USER=$NEW_USER,"${val}"
				;;
			-)
				case "${OPTARG}" in	
					user)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						[[ -z $NEW_USER ]] && NEW_USER="${val}" || NEW_USER=$NEW_USER,"${val}"
						;;
					user=*)
						val=${OPTARG#*=}
						[[ -z $NEW_USER ]] && NEW_USER="${val}" || NEW_USER=$NEW_USER,"${val}"
						;;
					debug)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						DEBUG=1
						;;
					*)
						_printUsage
				esac;;
			*)
				_printUsage
				;;
		esac
	done
    [[ -z $NEW_USER ]] && _error "Username argument is mandatory." && _printUsage
    [[ $NEW_USER =~ "," ]] && _error "Username should only be set once" && exit 1
```
This function is called at the beginning of the script. It permits to process arguments passed to the script. Lets go further...
```
function ProcessArgs()
{
    [[ $# -lt 2 ]] && _printUsage
```
$# contain the number of arguments passed to the function.
Our script requires at least 2 arguments: if the number of argument is less than two, we print script usage.
```
	while getopts ":u-:" optchar; do
		case "${optchar}" in
```
We check if each argument (optchar) passed to the script is valid depending (case) on its content using getopts command. getopts obtains options and their arguments from a list of defined parameters, here `u` and `-`._
I can't detail how getopts works, it will be too long and that's not the goal of this document. You can go to [https://www.shellscript.sh/tips/getopts/](https://www.shellscript.sh/tips/getopts/) to get more informations
```
		case "${optchar}" in
			u)
				val="${!OPTIND}"
				OPTIND=$(( $OPTIND + 1 ))
				[[ -z $NEW_USER ]] && NEW_USER="${val}" || NEW_USER=$NEW_USER,"${val}"
				;;
```
if argument *-u $NEW_USER* is passed to the script, we register NEW_USER variable.
val variable contains value of a variable whose name is contained in OPTIND variable. Please see bash indirection for details [https://riptutorial.com/bash/example/7567/parameter-indirection}(https://riptutorial.com/bash/example/7567/parameter-indirection) \
getopts OPTIND variable is then incremented by one, see getopts man.
We then increment OPTIND getopts builtin variable because we want next option to be process, not the actual option value.\
`[[ -z $NEW_USER ]]` returns 0 if NEW_USER variable is empty. We can then continue to next command with &&. If NEW_USER isn't empty, `[[ -z $NEW_USER ]]` will return 1 and command after || will be executed. As explained, we register NEW_USER as $val variable if NEW_USER is empty, or we register NEW_USER with its previous content and the new content.
```
			-)
				case "${OPTARG}" in	
```
getopts doesn't nativelly accept long arguments, but the - following another - is recognised as a single arg by getopts. We can pass long arguments adding a case loop. 
```
					user)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						[[ -z $NEW_USER ]] && NEW_USER="${val}" || NEW_USER=$NEW_USER,"${val}"
						;;
```
If *--user $NEW_USER* is passed we register NEW_USER var as we did before when *-u $NEW_USER*
```
					user=*)
						val=${OPTARG#*=}
						[[ -z $NEW_USER ]] && NEW_USER="${val}" || NEW_USER=$NEW_USER,"${val}"
						;;
```
If *--user=$NEW_USER* is passed to the script, we register NEW_USER var.
`${OPTARG#*=}` displays partially arg variable. Content after the # is removed from the variable. We see here that all (\*) content before = is removed. val has $NEW_USER value. Please see [https://www.cyberciti.biz/faq/bash-get-basename-of-filename-or-directory-name/](https://www.cyberciti.biz/faq/bash-get-basename-of-filename-or-directory-name/)\
```
            --debug)
                DEBUG="1"
            ;;
        esac
    done
```
If *--debug* is passed to the script, we set DEBUG var to 1 which means debugging is enabled. See _debug function.
```
			*)
				_printUsage
				;;
```
If argument passed to the script doesn't match any case value, we display the manual by calling _printUsage function.
```
    # check if user is defined
    [[ -z $NEW_USER ]] && _error "Username argument is mandatory." && _printUsage
    [[ $NEW_USER =~ "," ]] && _error "Username should only be set once" && exit 1
}
```
We now check if NEW_USER variable is empty as it shouldn't be. If so, we display a red message by calling _error function and we display script usage.\
If NEW_USER variable contains a comma, it means that more than one user argument has been passed to our script. We can only register one user at a time: we display a red message and exit.
