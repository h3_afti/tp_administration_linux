#!/bin/bash

_reset=$(tput sgr0)
_red=$(tput setaf 1)
_green=$(tput setaf 76)
_blue=$(tput setaf 38)

function _checkRootUser()
{
    if (( $EUID != 0 )); then
        echo "Please run as root or use sudo"
        exit 1
    fi
}

function _debug()
{
    if [[ $DEBUG == 1 ]]; then
        set -e
        set -x
        #trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
        #trap 'echo "\"${last_command}\" command ended with exit code $?."' EXIT
    fi
}

function _error()
{
    printf "${_red} %s${_reset}\n" "$@"
}

function _interact()
{
    printf "${_blue} %s${_reset}\n" "$@"
}

function _IsValidIp()
{
    local  ip=$1
    local  stat=1
    # Check if IP looks like an IP: 4 time 3 digits separated by dots
    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        # Internal field separator saved then set as dot so that we can use each ip part as argument
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

function _printSuccessMessage()
{
    _success "Website created successfully!"

    echo "################################################################"
    echo ""
    echo " >> Host      : ${VHOST}"
    echo " >> IP        : ${IP}"
    echo " >> User      : ${NEW_USER}"
    echo " >> Password  : ${USER_PASS}"
    echo " >> Database  : ${FILENAME}"
    echo " >> SQL host  : ${DB_HOST}"
    echo ""
    echo "################################################################"
}

function _printUsage()
{
    echo -n "$(basename $0) [OPTION]...

Create new website environment:
create user
create database
create virtualhost
create ftp access
add cms template if specified
Version $VERSION

Options:
mandaroty:
-u, --user        User
-d, --domain       Domain name
optional:
-H, --dbhost      MySQL Host
-c, --cms         CMS to use (joomla|wordpress)
-i, --ip          Webserver IP address. Can be useful for internal websites.
-h, --help        Display this help and exit
-v, --version     Output version information and exit
--debug           Activate debugging

Examples:
$(basename $0) --help
$(basename $0) -H localhost --user myuser -d website.tld -d www.website.tld -c joomla
"
    exit 1
}

function _success()
{
    printf "${_green} %s${_reset}\n" "$@"
}

function ConfigureFtpService()
{
    echo "${NEW_USER}" >> /etc/vsftpd.userlist
    systemctl reload vsftpd
}

function ConfigureWebServices()
{
    # Create nginx virtualhost from template
    case $CMS in
        joomla)
            envsubst '${VHOST} $ROOTDIR $FILENAME' < "/etc/nginx/sites-available/joomla.conf" > "/etc/nginx/sites-enabled/${FILENAME}.conf"
        ;;
        wordpress)
            envsubst '${VHOST} $ROOTDIR $FILENAME' < "/etc/nginx/sites-available/wordpress.conf" > "/etc/nginx/sites-enabled/${FILENAME}.conf"
            ConfigureWordpress
        ;;
        *)
            envsubst '${VHOST} $ROOTDIR $FILENAME' < "/etc/nginx/sites-available/noCMS.conf" > "/etc/nginx/sites-enabled/${FILENAME}.conf"
        ;;
    esac

    # Create php-fpm pool from template
    envsubst '$NEW_USER $FILENAME' < "/etc/php/7.3/fpm/defaultpool.conf" > "/etc/php/7.3/fpm/pool.d/${FILENAME}.conf"

	CreateSSLCertificate

    systemctl reload php7.3-fpm
    systemctl reload nginx
}

function ConfigureWordpress()
{
    # Configure first wordpress step
    cat > /home/${NEW_USER}/wp-config.php <<\EOF
<?php
define('DB_NAME', 'DATABASE_NAME');
define('DB_USER', 'SQL_USER');
define('DB_PASSWORD', 'SQL_PASSWD');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
$table_prefix = 'wp_';
define('WP_DEBUG', false);
if ( !defined('ABSPATH') )
define('ABSPATH', dirname(__FILE__) . '/');
require_once(ABSPATH . 'wp-settings.php');
EOF
    curl -s https://api.wordpress.org/secret-key/1.1/salt/ >> /home/${NEW_USER}/wp-config.php
    sed -i -e "s/DATABASE_NAME/${FILENAME}/" -e "s/SQL_USER/${NEW_USER}/" -e "s/SQL_PASSWD/${USER_PASS}/" /home/${NEW_USER}/wp-config.php
	chown ${NEW_USER}:www-data /home/${NEW_USER}/wp-config.php
	chmod 644 /home/${NEW_USER}/wp-config.php
}

function CreateMysqlDbUser()
{
    SQL1="CREATE DATABASE IF NOT EXISTS ${FILENAME};"
    SQL2="CREATE USER ${NEW_USER}@'%' IDENTIFIED BY '${USER_PASS}';"
    SQL3="GRANT ALL PRIVILEGES ON ${FILENAME}.* TO '${NEW_USER}'@'%';"
    SQL4="FLUSH PRIVILEGES;"

    mysql -h $DB_HOST -uroot -e "${SQL1}${SQL2}${SQL3}${SQL4}"

}

function CreateSSLCertificate()
{
# Create temp config file used by openssl to generate the certificate
certconfigfile=`mktemp`
cat > $certconfigfile <<\EOF
[req]
default_bits = 4096
prompt = no
default_md = sha256
req_extensions = req_ext
x509_extensions  = req_ext
distinguished_name = dn

[ dn ]
C=FR
ST=France
L=Paris
O=Security
OU=IT Department
emailAddress=admin
CN = My website

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
EOF

# Add all vhosts names as Subject Alternative Name in certificate
i=0
for host in ${VHOST};do
	((i=i+1))
	echo "DNS.${i}       = $host" >> $certconfigfile
done

# Generate the certificate
openssl req -config $certconfigfile -new -x509 -sha256 -newkey rsa:4096 -nodes -keyout /etc/nginx/certs/${FILENAME}.key -days 365 -out /etc/nginx/certs/${FILENAME}.crt 2>/dev/null
}

function CreateUser()
{
    case $CMS in
        joomla)
            useradd -m -s /bin/bash -g www-data -k /etc/skel.joomla ${NEW_USER}
            echo -e "${USER_PASS}\n${USER_PASS}" | passwd ${NEW_USER} 2>/dev/null
        ;;
        wordpress)
            useradd -m -s /bin/bash -g www-data -k /etc/skel.wordpress ${NEW_USER}
            echo -e "${USER_PASS}\n${USER_PASS}" | passwd ${NEW_USER} 2>/dev/null
        ;;
        *)
            useradd -m -s /bin/bash -g www-data ${NEW_USER}
            echo -e "${USER_PASS}\n${USER_PASS}" | passwd ${NEW_USER} 2>/dev/null
        ;;
    esac
}

function DefineVars()
{
    # add /usr/sbin to $PATH
    PATH="$PATH:/usr/sbin"

    #USER
    # check if user can be created with useradd: can only contain alphanumeric characters
    [[ "$NEW_USER" =~ [^a-zA-Z0-9$] ]] && _error "Username is invalid, please rerun the script with a valid username" && exit 1

    #Generate 16 char user password
    USER_PASS=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1`

    #Configuration files and database name
    FILENAME=`cut -d',' -f1 <<< $VHOSTS | tr '.' '_'`

    #DATABASE
    # if database host isn't defined, defaulting to localhost
    [[ ! $DB_HOST ]] && DB_HOST="localhost"

    #WEBSERVER ip will be shown in end report so that end user can configure DNS
    # if IP address isn't defined, defaulting to public ip
    [[ ! $IP ]] && IP=`dig +short myip.opendns.com @resolver1.opendns.com`

    # if IP isn't valid, defaulting to localhost
    [[ `_IsValidIp $IP` ]] && IP="127.0.0.1"

    #WEBSERVICES
    # nginx multiple server name has "domain.tld domain2.tld" format, but our $VHOSTS is "domain.tld,domain2.tld"
    VHOST=`echo $VHOSTS | tr ',' ' '`

    # export nginx and php vars
    export VHOST
    export NEW_USER
    export FILENAME
    export ROOTDIR=/home/${NEW_USER}
}

function Prerequisite()
{
    # Exit if user exists
    [[ `id -u ${NEW_USER} 2>/dev/null` ]] && _error "User ${NEW_USER} already exists. Please enter a new user" && exit 1

    # Exit if website exists
    for HOST in `echo $VHOSTS | tr ',' ' '`; do
        [[ `grep -w "$HOST" /etc/Users_Websites.txt` ]] && _error "Virtual host $HOST already exists" && exit 1
    done

	# Add our new user and website to the list
    echo "$NEW_USER:$VHOSTS" >> /etc/Users_Websites.txt
}

function ProcessArgs()
{
    [[ $# -lt 2 ]] && _printUsage
	while getopts ":ducHi-:" optchar; do
		case "${optchar}" in
			-)
				case "${OPTARG}" in
					domain)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						[[ -z $VHOSTS ]] && VHOSTS="${val}" || VHOSTS=$VHOSTS,"${val}"
						;;
					domain=*)
						val=${OPTARG#*=}
						[[ -z $VHOSTS ]] && VHOSTS="${val}" || VHOSTS=$VHOSTS,"${val}"
						;;
					user)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						[[ -z $NEW_USER ]] && NEW_USER="${val}" || NEW_USER=$NEW_USER,"${val}"
						;;
					user=*)
						val=${OPTARG#*=}
						[[ -z $NEW_USER ]] && NEW_USER="${val}" || NEW_USER=$NEW_USER,"${val}"
						;;
					cms)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						[[ -z $CMS ]] && CMS="${val}" || CMS=$CMS,"${val}"
						;;
					cms=*)
						val=${OPTARG#*=}
						[[ -z $CMS ]] && CMS="${val}" || CMS=$CMS,"${val}"
						;;
					dbhost)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						[[ -z $DB_HOST ]] && DB_HOST="${val}" || DB_HOST=$DB_HOST,"${val}"
						;;
					dbhost=*)
						val=${OPTARG#*=}
						[[ -z $DB_HOST ]] && DB_HOST="${val}" || DB_HOST=$DB_HOST,"${val}"
						;;
					ip)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						[[ -z "$IP" ]] && IP="${val}" || IP=$IP,"${val}"
						;;
					ip=*)
						val=${OPTARG#*=}
						[[ -z "$IP" ]] && IP="${val}" || IP=$IP,"${val}"
						;;
					debug)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						DEBUG=1
						;;
					debug=*)
						val=${OPTARG#*=}
						DEBUG=1
						;;
					*)
						_printUsage
				esac;;
			d)
				val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
				[[ -z $VHOSTS ]] && VHOSTS="${val}" || VHOSTS=$VHOSTS,"${val}"
				;;
			u)
				val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
				[[ -z $NEW_USER ]] && NEW_USER="${val}" || NEW_USER=$NEW_USER,"${val}"
				;;
			c)
				val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
				[[ -z $CMS ]] && CMS="${val}" || CMS=$CMS,"${val}"
				;;
			H)
				val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
				[[ -z $DB_HOST ]] && DB_HOST="${val}" || DB_HOST=$DB_HOST,"${val}"
				;;
			i)
				val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
				[[ -z $IP ]] && IP="${val}" || IP=$IP,"${val}"
				;;
			*)
				_printUsage
				;;
		esac
	done
    # check if user is defined
    [[ -z $NEW_USER ]] && _error "Username argument is mandatory." && _printUsage
    [[ $NEW_USER =~ "," ]] && _error "Username should only be set once" && exit 1
    [[ -z $VHOSTS ]] && _error "Domain name argument is mandatory." && _printUsage
    [[ $DB_HOST =~ "," ]] && _error "DB host should only be set once" && exit 1
    [[ $CMS =~ "," ]] && _error "DMS should only be set once" && exit 1
}

function UnsetVar()
{
    unset VHOST
    unset NEW_USER
    unset FILENAME
    unset ROOTDIR
}

#### MAIN ####
function main()
{
    DefineVars

    _success "Creating user..."
    CreateUser
    _success "Done!"

    _success "Creating DB..."
    CreateMysqlDbUser
    _success "Done!"

    _success "Configuring web services..."
    ConfigureWebServices
    _success "Done!"

    _success "Configuring ftp services..."
    ConfigureFtpService
    _success "Done!"

    _printSuccessMessage

    UnsetVar

    exit 0
}

VERSION="1.0"
PATH="$PATH:/usr/sbin"

_checkRootUser

ProcessArgs "$@"

_debug

Prerequisite

main "$@"
