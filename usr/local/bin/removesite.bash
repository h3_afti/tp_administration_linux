#!/bin/bash

_reset=$(tput sgr0)
_red=$(tput setaf 1)
_yellow=$(tput setaf 3)
_green=$(tput setaf 76)
_blue=$(tput setaf 38)

function _checkRootUser()
{
    if (( $EUID != 0 )); then
        echo "Please run as root or use sudo"
        exit 1
    fi
}

function _debug()
{
    if [[ $DEBUG == 1 ]]; then
        set -e
        set -x
        #trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
        #trap 'echo "\"${last_command}\" command ended with exit code $?."' EXIT
    fi
}

function _error()
{
    printf "${_red} %s${_reset}\n" "$@"
}

function _interact()
{
    printf "${_blue} %s${_reset}\n" "$@"
}

function _printSuccessMessage()
{
    _success "Website deleted successfully!"
}

function _printUsage()
{
    echo -n "$(basename $0) [OPTION]...

Remove website environment:
delete user
delete database
delete virtualhost
delete ftp access
Version $VERSION

Options:
mandaroty:
-u, --user        User
-d, --domain      Domain name
optional:
-H, --dbhost      MySQL Host
-h, --help        Display this help and exit
-v, --version     Output version information and exit
--debug           Activate debugging

Examples:
$(basename $0) --help
$(basename $0) -H localhost --user myuser -d website.tld
"
    exit 1
}

function _success()
{
    printf "${_green} %s${_reset}\n" "$@"
}

function _warning()
{
    printf "${_yellow} %s${_reset}\n" "$@"
}

function DefineVars()
{
    # add /usr/sbin to $PATH
    PATH="$PATH:/usr/sbin"

    #Grab configuration files and database name from our file
    FILENAME=`grep -P "(:|,)$VHOST(,|$)" /etc/Users_Websites.txt | cut -d':' -f2 | cut -d',' -f1 | tr '.' '_'`
}

function DeleteUser()
{
    # check if user exists in system
    if [ `getent passwd ${NEW_USER} >/dev/null;echo $?` -eq 0 ];then
        killall -u ${NEW_USER}
        userdel -r -f ${NEW_USER} 2>/dev/null
        sed -i "/^$NEW_USER/d" /etc/Users_Websites.txt
    else
        _warning "User ${NEW_USER} doesn't exists in system. User can't be removed"
    fi
}

function ProcessArgs()
{
    _UnsetVar
    [[ $# -lt 1 ]] && _printUsage
	while getopts ":duH-:" optchar; do
		case "${optchar}" in
			-)
				case "${OPTARG}" in
					domain)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						[[ -z $VHOST ]] && VHOST="${val}" || VHOST=$VHOST,"${val}"
						;;
					domain=*)
						val=${OPTARG#*=}
						[[ -z $VHOST ]] && VHOST="${val}" || VHOST=$VHOST,"${val}"
						;;
					user)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						[[ -z $NEW_USER ]] && NEW_USER="${val}" || NEW_USER=$NEW_USER,"${val}"
						;;
					user=*)
						val=${OPTARG#*=}
						[[ -z $NEW_USER ]] && NEW_USER="${val}" || NEW_USER=$NEW_USER,"${val}"
						;;
					dbhost)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						[[ -z $DB_HOST ]] && DB_HOST="${val}" || DB_HOST=$DB_HOST,"${val}"
						;;
					dbhost=*)
						val=${OPTARG#*=}
						[[ -z $DB_HOST ]] && DB_HOST="${val}" || DB_HOST=$DB_HOST,"${val}"
						;;
					debug)
						val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
						DEBUG=1
						;;
					debug=*)
						val=${OPTARG#*=}
						DEBUG=1
						;;
					*)
						_printUsage
				esac;;
			d)
				val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
				[[ -z $VHOST ]] && VHOST="${val}" || VHOST=$VHOST,"${val}"
				;;
			u)
				val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
				[[ -z $NEW_USER ]] && NEW_USER="${val}" || NEW_USER=$NEW_USER,"${val}"
				;;
			H)
				val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
				[[ -z $DB_HOST ]] && DB_HOST="${val}" || DB_HOST=$DB_HOST,"${val}"
				;;
				*)
				_printUsage
				;;
		esac
	done

    # check if user or domain are defined
    [[ -z $NEW_USER && -z $VHOST ]] && _error "Username or Domain can't be empty" && _printUsage

    # Args can only be set once
    [[ $NEW_USER =~ "," ]] && _error "Username should only be set once" && exit 1
    [[ $VHOST =~ "," ]] && _error "Domain should only be set once" && exit 1
    [[ $DB_HOST =~ "," ]] && _error "DB host should only be set once" && exit 1

    # if database host isn't defined, defaulting to localhost
    [[ -z $DB_HOST ]] && DB_HOST="localhost"

    # Check if user or vhost provided exists
    [[ ! -z $NEW_USER ]] && [[ `grep -P "^${NEW_USER}:" /etc/Users_Websites.txt; echo $?` == "1" ]] && _error "User $NEW_USER doesn't exist" && exit 1
    [[ ! -z $VHOST ]] && [[ `grep -P "(:|,)${VHOST}(,|$)" /etc/Users_Websites.txt; echo $?` == "1" ]] && _error "Domain $VHOST doesn't exist" && exit 1

    # Check if vhost and user match
    [[ ! -z $NEW_USER || ! -z $VHOST ]] && [[ `grep -P "^${NEW_USER}:.*${VHOST}(,|$)" /etc/Users_Websites.txt; echo $?` == 1 ]] && _error "User $NEW_USER doesn't match with $VHOST provided. Exiting" && exit 1

    # If only provinding user, vhost is undefined: we define it based on /etc/Users.txt file listing all users and domain named associated and ask for removal confirmation
    [[ -z $VHOST ]] && VHOST=`grep -P "^$NEW_USER:" /etc/Users_Websites.txt |cut -d':' -f2`

    # If only provinding vhost, user is undefined: we define it based on /etc/Users.txt file listing all users and domain named associated and ask for removal confirmation
    [[ -z $NEW_USER ]] && NEW_USER=`grep -P "^$VHOST:" /etc/Users_Websites.txt |cut -d':' -f1`

    # Ask for confirmation
    _interact "Enter YES if you want to remove user ${NEW_USER} and ${VHOST} website:"
    read var
    [[ "$var" != "YES" ]] && _error "Operation cancelled by user" && exit 1
}

function RemoveFtpService()
{
    sed -i "/^${NEW_USER}/d" /etc/vsftpd.userlist
    systemctl reload vsftpd
}

function RemoveMysqlDbUser()
{
    SQL1="DROP DATABASE IF EXISTS ${FILENAME};"
    SQL2="DROP USER IF EXISTS ${NEW_USER}@'%';"
    SQL3="FLUSH PRIVILEGES;"

    mysql -f -h $DB_HOST -uroot -e "${SQL1}${SQL2}${SQL3}"
}

function RemoveWebServices()
{
    # Remove nginx virtualhost
    rm -f "/etc/nginx/sites-enabled/${FILENAME}.conf"

    # Remove php-fpm pool
    rm -f "/etc/php/7.3/fpm/pool.d/${FILENAME}.conf"

    # Don't remove logs but archinving them instead
    [[ -f "/var/log/nginx/${FILENAME}.access.log" ]] && mv /var/log/nginx/${FILENAME}.access.log /var/log/nginx/archives/${FILENAME}_${NEW_USER}.access.log`date '+%Y-%m-%d_%H:%M:%S'`
    [[ -f "/var/log/nginx/${FILENAME}.error.log" ]] && mv /var/log/nginx/${FILENAME}.error.log /var/log/nginx/archives/${FILENAME}_${NEW_USER}.error.log`date '+%Y-%m-%d_%H:%M:%S'`
    [[ -f "/var/log/php/${FILENAME}.access.log" ]] && mv /var/log/php/${FILENAME}.access.log /var/log/php/archives/${FILENAME}_${NEW_USER}.access.log`date '+%Y-%m-%d_%H:%M:%S'`
    [[ -f "/var/log/php/${FILENAME}.error.log" ]] && mv /var/log/php/${FILENAME}.error.log /var/log/php/archives/${FILENAME}_${NEW_USER}.error.log`date '+%Y-%m-%d_%H:%M:%S'`

    systemctl reload nginx
    systemctl reload php7.3-fpm
}

function _UnsetVar()
{
    unset DB_HOST
    unset VHOST
    unset NEW_USER
    unset DEBUG
	unset var
}

#### MAIN ####
function main()
{
    [[ $# -lt 1 ]] && _printUsage

    DefineVars

    _success "Deleting user and website..."
    DeleteUser
    _success "Done!"

    _success "Removing website DB..."
    RemoveMysqlDbUser
    _success "Done!"

    _success "Removing web service host..."
    RemoveWebServices
    _success "Done!"

    _success "Removing ftp access..."
    RemoveFtpService
    _success "Done!"

    _printSuccessMessage

    _UnsetVar

    exit 0
}

VERSION="1.0"
PATH="$PATH:/usr/sbin"

_checkRootUser

ProcessArgs "$@"

_debug

main "$@"
